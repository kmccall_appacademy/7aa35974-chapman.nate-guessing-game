# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.


def guessing_game

  # Don't want the computer to choose 0
  number = (100 * rand).floor + 1
  guesses = 0
  user_guess = 0
  while number != user_guess

    puts "Guess a number from 1 to 100!"
    user_guess = gets.chomp.to_i

    guesses += 1
    if user_guess > number
      puts "#{user_guess.to_i} too high."
    elsif user_guess < number
      puts "#{user_guess.to_i} too low."
    end

  end
  puts "You guessed the correct number: #{number}!"
  puts "It took you #{guesses} guess(es)!"

end

def file_shuffler
  puts "What is the name of the file you wish to shuffle?"
  input_name = gets.chomp
  file_line_array = File.readlines(input_name)
  shuffled = file_line_array.shuffle
  new_file = File.open("#{input_name[0..input_name.size - 5]}-shuffled.txt", 'w')
  shuffled.each do |line|
    new_file.puts line
  end
  new_file.close

end
